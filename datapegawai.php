<?php require_once('Connections/connmyapps.php'); ?>
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "ADMIN,USER";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php?pesan=Sorry, No Privilege [error 212]";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($theValue) : mysqli_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rspegawai = 5;
$pageNum_rspegawai = 0;
if (isset($_GET['pageNum_rspegawai'])) {
  $pageNum_rspegawai = $_GET['pageNum_rspegawai'];
}
$startRow_rspegawai = $pageNum_rspegawai * $maxRows_rspegawai;

mysqli_select_db($connmyapps,$database_connmyapps );
$query_rspegawai = "SELECT idpegawai, namapegawai, kotapegawai, gajipegawai FROM tbpegawai ORDER BY idpegawai DESC";
$query_limit_rspegawai = sprintf("%s LIMIT %d, %d", $query_rspegawai, $startRow_rspegawai, $maxRows_rspegawai);
$rspegawai = mysqli_query($connmyapps,$query_limit_rspegawai) or die(mysqli_error());
$row_rspegawai = mysqli_fetch_assoc($rspegawai);

if (isset($_GET['totalRows_rspegawai'])) {
  $totalRows_rspegawai = $_GET['totalRows_rspegawai'];
} else {
  $all_rspegawai = mysqli_query($connmyapps,$query_rspegawai);
  $totalRows_rspegawai = mysqli_num_rows($all_rspegawai);
}
$totalPages_rspegawai = ceil($totalRows_rspegawai/$maxRows_rspegawai)-1;

$queryString_rspegawai = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rspegawai") == false && 
        stristr($param, "totalRows_rspegawai") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rspegawai = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rspegawai = sprintf("&totalRows_rspegawai=%d%s", $totalRows_rspegawai, $queryString_rspegawai);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include("bootstrap.php"); ?>
<title>Data Pegawai</title>
</head>

<body>
<div class="container">
  <div class="jumbotron">
    <p class="sukses"><?php if(!empty($_GET['pesan'])) { echo $_GET['pesan']; } ?></p>
    <h1>DATA PEGAWAI</h1>
    <p><a href="insertpegawai.php" class="btn btn-success">INSERT PEGAWAI</a> &nbsp; <a href="updategajipegawai.php"  onClick="javascript:return confirm('Semua gaji pegawai akan naik 10%, setelah update proses tidak bisa dibatalkan !')" class="btn btn-success">UPDATE 10% GAJI</a> &nbsp; <a href="<?php echo $logoutAction ?>" class="btn btn-warning">LOGOUT</a></p>
  
  </div>
  <h2>&raquo;  LIST DATA PEGAWAI CV.DUTA PURNAMA</h2>
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td><table border="0">
          <tr>
            <td><?php if ($pageNum_rspegawai > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rspegawai=%d%s", $currentPage, 0, $queryString_rspegawai); ?>">First</a>
            <?php } // Show if not first page ?></td>
            <td><?php if ($pageNum_rspegawai > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rspegawai=%d%s", $currentPage, max(0, $pageNum_rspegawai - 1), $queryString_rspegawai); ?>">Previous</a>
            <?php } // Show if not first page ?></td>
            <td><?php if ($pageNum_rspegawai < $totalPages_rspegawai) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rspegawai=%d%s", $currentPage, min($totalPages_rspegawai, $pageNum_rspegawai + 1), $queryString_rspegawai); ?>">Next</a>
            <?php } // Show if not last page ?></td>
            <td><?php if ($pageNum_rspegawai < $totalPages_rspegawai) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rspegawai=%d%s", $currentPage, $totalPages_rspegawai, $queryString_rspegawai); ?>">Last</a>
            <?php } // Show if not last page ?></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;
Records <?php echo ($startRow_rspegawai + 1) ?> to <?php echo min($startRow_rspegawai + $maxRows_rspegawai, $totalRows_rspegawai) ?> of <?php echo $totalRows_rspegawai ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="1" cellpadding="2" cellspacing="2" class="table table-hover">
    <tr align="center" bgcolor="#CCFF99">
      <td width="15%"><strong>id pegawai</strong></td>
      <td width="24%"><strong>Nama Pegawai</strong></td>
      <td width="16%"><strong>Kota Pegawai</strong></td>
      <td width="20%"><strong>Gaji Pegawai</strong></td>
      <td width="25%"><strong>Admin</strong></td>
    </tr>
    <?php do { ?>
      <tr>
        <td><?php echo $row_rspegawai['idpegawai']; ?></td>
        <td><?php echo $row_rspegawai['namapegawai']; ?></td>
        <td><?php echo $row_rspegawai['kotapegawai']; ?></td>
        <td align="right"><table width="100%" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td align="left"><strong>Rp</strong></td>
            <td align="right"><strong><?php echo $row_rspegawai['gajipegawai']; ?></strong></td>
          </tr>
        </table></td>
        <td align="center"><a href="detailpegawai.php?id=<?php echo $row_rspegawai['idpegawai']; ?>" class="btn btn-info">DETAIL</a> &nbsp; <a href="updatepegawai.php?idp=<?php echo $row_rspegawai['idpegawai']; ?>" class="btn btn-warning">EDIT</a> &nbsp; <a href="deletepegawai.php?idd=<?php echo $row_rspegawai['idpegawai']; ?>" class="btn btn-danger" onClick="javascript:return confirm('yakin hapus data <?php echo $row_rspegawai['namapegawai']; ?> ?')">DEL</a></td>
      </tr>
      <?php } while ($row_rspegawai = mysqli_fetch_assoc($rspegawai)); ?>
  </table>
</div>
</body>
</html>
<?php
mysqli_free_result($rspegawai);
?>
