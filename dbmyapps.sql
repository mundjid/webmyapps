-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 18, 2021 at 02:45 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbmapps`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblogin`
--

DROP TABLE IF EXISTS `tblogin`;
CREATE TABLE IF NOT EXISTS `tblogin` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` varchar(10) NOT NULL,
  `passuser` varchar(10) NOT NULL,
  `leveluser` varchar(10) NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`idlogin`),
  UNIQUE KEY `iduser` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tblogin`
--

INSERT INTO `tblogin` (`idlogin`, `iduser`, `passuser`, `leveluser`) VALUES
(1, 'admin', 'admin123', 'ADMIN'),
(2, 'user', 'user123', 'USER'),
(3, 'purnama', 'purnama123', 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `tbpegawai`
--

DROP TABLE IF EXISTS `tbpegawai`;
CREATE TABLE IF NOT EXISTS `tbpegawai` (
  `idpegawai` int(11) NOT NULL AUTO_INCREMENT,
  `namapegawai` varchar(30) NOT NULL,
  `kotapegawai` varchar(30) NOT NULL,
  `tanggallahirpegawai` date NOT NULL,
  `gajipegawai` int(11) NOT NULL DEFAULT '0',
  `detailpegawai` text,
  PRIMARY KEY (`idpegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbpegawai`
--

INSERT INTO `tbpegawai` (`idpegawai`, `namapegawai`, `kotapegawai`, `tanggallahirpegawai`, `gajipegawai`, `detailpegawai`) VALUES
(2, 'Devina Zahra', 'JAKARTA', '2001-10-13', 8784600, 'Devina Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequa'),
(3, 'Darrel Gani', 'BANDUNG', '1996-10-05', 6588450, 'dARREL is Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequa'),
(4, 'DINA MAYASARI DEW', 'JAKARTA', '1978-09-28', 8784600, 'Dina Mayasari adalah pegawai terbaik, saat ini dibagian Finance'),
(5, 'EKO PURNAMA', 'BANDUNG', '1980-09-20', 6050000, 'EKO PURNAMA lahir di medan');
