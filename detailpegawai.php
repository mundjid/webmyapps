<?php require_once('Connections/connmyapps.php'); global $connmyapps; ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "ADMIN,USER";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php?pesan=Sorry, No Privilege ! [error 212]";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
global $connmyapps;
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
  {
    global $connmyapps;
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
  
    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($connmyapps,$theValue) : mysqli_escape_string($connmyapps,$theValue);
  
    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;    
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}

$colname_rsdetailpegawai = "-1";
if (isset($_GET['id'])) {
  $colname_rsdetailpegawai = $_GET['id'];
}
mysqli_select_db($connmyapps,$database_connmyapps);
$query_rsdetailpegawai = sprintf("SELECT * FROM tbpegawai WHERE idpegawai = %s", GetSQLValueString($colname_rsdetailpegawai, "int"));
$rsdetailpegawai = mysqli_query($connmyapps,$query_rsdetailpegawai) or die(mysqli_error());
$row_rsdetailpegawai = mysqli_fetch_assoc($rsdetailpegawai);
$totalRows_rsdetailpegawai = mysqli_num_rows($rsdetailpegawai);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include("bootstrap.php"); ?>
<title>Detail Pegawai</title>
</head>

<body>
<div class="container">
  <div class="jumbotron">
    <h1>Detail Pegawai</h1>
    <?php if ($totalRows_rsdetailpegawai == 0) { // Show if recordset empty ?>
      <table width="100%" border="0" cellspacing="2" cellpadding="2">
        <tr>
          <td class="error">Maaf data pegawai tidak ada !</td>
        </tr>
      </table>
      <?php } // Show if recordset empty ?>
<p>&nbsp;</p>
    <?php if ($totalRows_rsdetailpegawai > 0) { // Show if recordset not empty ?>
      <table width="90%" border="0" align="center" cellpadding="2" cellspacing="4">
        <tr>
          <td width="19%"><strong>Id Pegawai</strong></td>
          <td width="81%"><strong><?php echo $row_rsdetailpegawai['idpegawai']; ?></strong></td>
        </tr>
        <tr>
          <td><strong>Nama Pegawai</strong></td>
          <td><strong><?php echo $row_rsdetailpegawai['namapegawai']; ?></strong></td>
        </tr>
        <tr>
          <td><strong>Kota </strong></td>
          <td><?php echo $row_rsdetailpegawai['kotapegawai']; ?></td>
        </tr>
        <tr>
          <td><strong>Tanggal Lahir</strong></td>
          <td><?php echo $row_rsdetailpegawai['tanggallahirpegawai']; ?></td>
        </tr>
        <tr>
          <td><strong>Gaji Pegawai</strong></td>
          <td><?php echo $row_rsdetailpegawai['gajipegawai']; ?></td>
        </tr>
        <tr>
          <td><strong>Detail Pegawai</strong></td>
          <td><em><?php echo $row_rsdetailpegawai['detailpegawai']; ?></em></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><a href="datapegawai.php" class="btn btn-info" >&laquo; Back</a></td>
        </tr>
      </table>
      <?php } // Show if recordset not empty ?>
  </div>
 
</div>
</body>
</html>
<?php
mysqli_free_result($rsdetailpegawai);
?>
