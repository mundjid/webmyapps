<?php require_once('Connections/connmyapps.php'); global $connmyapps;?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "ADMIN";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "datapegawai.php?pesan=Sorry, No Privilege ! [error 212]";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
global $connmyapps;
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
  {
    global $connmyapps;
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
  
    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($connmyapps,$theValue) : mysqli_escape_string($connmyapps,$theValue);
  
    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;    
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
  }

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO tbpegawai (namapegawai, kotapegawai, tanggallahirpegawai, gajipegawai, detailpegawai) VALUES (%s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['namapegawai'], "text"),
                       GetSQLValueString($_POST['kotapegawai'], "text"),
                       GetSQLValueString($_POST['tanggallahirpegawai'], "date"),
                       GetSQLValueString($_POST['gajipegawai'], "int"),
                       GetSQLValueString($_POST['detailpegawai'], "text"));

  mysqli_select_db($connmyapps,$database_connmyapps);
  $Result1 = mysqli_query($connmyapps,$insertSQL ) or die(mysqli_error());

  $insertGoTo = "datapegawai.php?pesan= Insert Pegawai Success";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include("bootstrap.php"); ?>
<title>Insert Pegawai</title>
<script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>
</head>

<body>
<div class="jumbotron">
  <h1 class="display-4">Insert Pegawai</h1>
  <p class="lead"></p>
  <p class="lead"> <a href="datapegawai.php" class="btn btn-info">&laquo; Back</a></p>
  <hr class="my-4">
  <p>Tambah Data Pegawai CV. Duta Purnama</p>
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td>&nbsp;
        <form action="<?php echo $editFormAction; ?>" method="post" name="form1" onSubmit="MM_validateForm('namapegawai','','R','tanggallahirpegawai','','R','gajipegawai','','RisNum');return document.MM_returnValue">
          <table width="90%" align="center">
            <tr valign="baseline">
              <td width="15%" align="right" nowrap>Nama Pegawai:</td>
              <td width="85%"><input name="namapegawai" type="text" id="namapegawai" value="" size="32" maxlength="30" class="form-control"></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right">Kota Pegawai:</td>
              <td><select name="kotapegawai" class="form-control">
                <option value="BANDUNG" <?php if (!(strcmp("BANDUNG", ""))) {echo "SELECTED";} ?>>BANDUNG</option>
                <option value="BEKASI" <?php if (!(strcmp("BEKASI", ""))) {echo "SELECTED";} ?>>BEKASI</option>
                <option value="JAKARTA" <?php if (!(strcmp("JAKARTA", ""))) {echo "SELECTED";} ?>>JAKARTA</option>
              </select></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right">Tanggal lahir:</td>
              <td><input name="tanggallahirpegawai" type="text" class="form-control" id="tanggallahirpegawai" placeholder="YYYY-MM-DD" value="" size="32" maxlength="10"></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right">Gaji pegawai Rp. :</td>
              <td><input name="gajipegawai" type="text" id="gajipegawai" value="" size="32" class="form-control"></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right" valign="top">Detail pegawai:</td>
              <td><textarea name="detailpegawai" cols="50" rows="5" class="form-control"></textarea></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right">&nbsp;</td>
              <td><input type="submit" value="Insert Pegawai" class="btn btn-success"></td>
            </tr>
          </table>
          <input type="hidden" name="MM_insert" value="form1">
        </form>
      <p>&nbsp;</p></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p class="lead">
   
  </p>
</div>
</body>
</html>