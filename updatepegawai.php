<?php require_once('Connections/connmyapps.php'); global $connmyapps;?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "ADMIN";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "datapegawai.php?pesan=Sorry, No Privilege ! [error 212]";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
global $connmyapps;
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
  {
    global $connmyapps;
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
  
    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($connmyapps,$theValue) : mysqli_escape_string($connmyapps,$theValue);
  
    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;    
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
  }

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE tbpegawai SET namapegawai=%s, kotapegawai=%s, tanggallahirpegawai=%s, gajipegawai=%s, detailpegawai=%s WHERE idpegawai=%s",
                       GetSQLValueString($_POST['namapegawai'], "text"),
                       GetSQLValueString($_POST['kotapegawai'], "text"),
                       GetSQLValueString($_POST['tanggallahirpegawai'], "date"),
                       GetSQLValueString($_POST['gajipegawai'], "int"),
                       GetSQLValueString($_POST['detailpegawai'], "text"),
                       GetSQLValueString($_POST['idpegawai'], "int"));

  mysqli_select_db($connmyapps,$database_connmyapps );
  $Result1 = mysqli_query($connmyapps,$updateSQL) or die(mysqli_error($connmyapps));

  $updateGoTo = "datapegawai.php?pesan=Edit Pegawai Success!";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rseditpegawai = "-1";
if (isset($_GET['idp'])) {
  $colname_rseditpegawai = $_GET['idp'];
}
mysqli_select_db($connmyapps,$database_connmyapps);
$query_rseditpegawai = sprintf("SELECT * FROM tbpegawai WHERE idpegawai = %s", GetSQLValueString($colname_rseditpegawai, "int"));
$rseditpegawai = mysqli_query($connmyapps,$query_rseditpegawai) or die(mysqli_error($connmyapps));
$row_rseditpegawai = mysqli_fetch_assoc($rseditpegawai);
$totalRows_rseditpegawai = mysqli_num_rows($rseditpegawai);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include("bootstrap.php"); ?>
<title>Update Pegawai</title>
<script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>
</head>

<body>
<div class="jumbotron">
  <h1 class="display-4">Edit Data Pegawai </h1>
  <p><a href="datapegawai.php" class="btn btn-info">&laquo; Back</a></p>

  <hr class="my-4">
  <p>Edit data pegawai CV. Duta Purnama</p>
  <?php if ($totalRows_rseditpegawai == 0) { // Show if recordset empty ?>
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td>Maaf . Data pegawai tidak tersedia !</td>
      </tr>
    </table>
    <?php } // Show if recordset empty ?>
<p>&nbsp;</p>
  <form action="<?php echo $editFormAction; ?>" method="post" name="form1" onSubmit="MM_validateForm('namapegawai','','R','tanggallahirpegawai','','R','gajipegawai','','RisNum');return document.MM_returnValue">
    <?php if ($totalRows_rseditpegawai > 0) { // Show if recordset not empty ?>
      <table width="90%" align="center">
        <tr valign="baseline">
          <td nowrap align="right">Nama Pegawai:</td>
          <td><input name="namapegawai" type="text" class="form-control" id="namapegawai" value="<?php echo htmlentities($row_rseditpegawai['namapegawai'], ENT_COMPAT, 'utf-8'); ?>" size="32" maxlength="30"></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">Kota Pegawai:</td>
          <td><select name="kotapegawai" class="form-control">
            <option value="BANDUNG" <?php if (!(strcmp("BANDUNG", htmlentities($row_rseditpegawai['kotapegawai'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>BANDUNG</option>
            <option value="BEKASI" <?php if (!(strcmp("BEKASI", htmlentities($row_rseditpegawai['kotapegawai'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>BEKASI</option>
            <option value="JAKARTA" <?php if (!(strcmp("JAKARTA", htmlentities($row_rseditpegawai['kotapegawai'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>JAKARTA</option>
          </select></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">Tanggal Lahir :</td>
          <td><input name="tanggallahirpegawai" type="text" class="form-control" id="tanggallahirpegawai" placeholder="YYYY-MM-DD" value="<?php echo htmlentities($row_rseditpegawai['tanggallahirpegawai'], ENT_COMPAT, 'utf-8'); ?>" size="32" maxlength="11"></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">Gaji Pegawa Rp.i:</td>
          <td><input name="gajipegawai" type="text" class="form-control" id="gajipegawai" value="<?php echo htmlentities($row_rseditpegawai['gajipegawai'], ENT_COMPAT, 'utf-8'); ?>" size="32"></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right" valign="top">Detail Pegawai:</td>
          <td><textarea name="detailpegawai" cols="50" rows="5"><?php echo htmlentities($row_rseditpegawai['detailpegawai'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">&nbsp;</td>
          <td><input type="submit" value="Update Pegawai" class="btn btn-info"></td>
        </tr>
      </table>
      <?php } // Show if recordset not empty ?>
<input type="hidden" name="idpegawai" value="<?php echo $row_rseditpegawai['idpegawai']; ?>">
    <input type="hidden" name="MM_update" value="form1">
    <input type="hidden" name="idpegawai" value="<?php echo $row_rseditpegawai['idpegawai']; ?>">
  </form>
  <p>&nbsp;</p>
</div>
</body>
</html>
<?php
mysqli_free_result($rseditpegawai);
?>
